# Importing required libs
import numpy as np

# EV learning agent class
class EV_Agent:

    def __init__(self, name, start_time, end_time, charge_required,rank, daily_price, tau, solar_data):
        #Agent's initializaion: 
        # agent_name: Agent's name
        # start_time: Arrival time of the EV agent
        # end_time: Departure time of the EV agent
        # resource_required: Amount of desired resource by the agent (in terms of number of instants)
        # instants: Total number of decision instants 
        # ev_naive_charging_policy: naive charging policy of the EV agent
        #Thompsong Sampling learning algorithm parameters:
        # theta: Vector of unknown variable corresponding to each decision instant
        # mu: Vetor of estimated mean of each element in the theta vector
        # tau: Vector of standard deviation in each element in the theta vector
        # n: Vector of number of times each element (arm) of theta vector has been played
        # Q: Vector of observed Q-values of each element in the theta vector
        # alpha: learning parameter of the Thompson Sampling algorithm
        self.name = name
        self.start_time = start_time
        self.end_time = end_time
        self.sort_param = (end_time-start_time)
        self.charge_required = charge_required
        self.rank = rank
        self.price_data = daily_price
        self.available_slots = np.linspace(0,1439,1440)
        self.taken_slots_message = np.array([])
        self.theta = np.zeros(1440,dtype=float)
        self.solar_data = solar_data
        self.ini = 0
        self.n = np.array([0]*len(self.theta),dtype=int)
        self.tau = np.array([tau]*len(self.theta),dtype=float)
        self.tau0 = np.array([2]*len(self.theta),dtype=float)
        self.mu0 = np.array([0]*len(self.theta),dtype=float)
        self.mu0 = 1- self.price_data
        self.Q = np.array([0]*len(self.theta),dtype=float)
        self.remaining_chrg = 0
        self.theta_solar = np.zeros(1440,dtype=float)
        self.n_solar = np.array([0]*len(self.theta_solar),dtype=int)
        self.tau_solar = np.array([100]*len(self.theta_solar),dtype=float)
        self.tau0_solar = np.array([5]*len(self.theta_solar),dtype=float)
        self.mu0_solar = np.array([0]*len(self.theta_solar),dtype=float)
        self.Q_solar = np.array([0]*len(self.theta_solar),dtype=float)
        self.estimate_theta_solar()
        self.estimate_theta()
        self.inst_voltage = 1.0
        self.cong = False
        self.this_reward = 0
        self.solar_estimate = np.zeros(1440,dtype=float)
        self.reward_history = []
        self.reward_historyv = []
        self.reward_historye = []

    # Function to estimate theta solar
    def estimate_theta_solar(self):
        for i in range(0,len(self.theta)):
            if ((i >= self.start_time) or (i <= self.end_time)):
                self.theta_solar[i] = np.mean(np.random.normal(self.solar_data[i], (1/self.tau0_solar[i]), 50))
            else:
                self.theta_solar[i] = np.mean(np.random.normal(0, (1/1000), 5))

    # Function to estimate theta
    def estimate_theta(self):
        for i in range(0,len(self.theta)):
            if ((i >= self.start_time) or (i <= self.end_time)):
                self.theta[i] = np.mean(np.random.normal(1-(self.price_data[i]/np.max(self.price_data)), (1/self.tau0[i]), 50))
            else:
                self.theta[i] = np.mean(np.random.normal(-10, (1/100), 5))
                
    # Function to select actions
    def select_actions(self):
        self.solar_estimate = (self.theta > 0) * self.theta_solar
        self.remaining_chrg = max(int(np.ceil(self.charge_required - (np.sum(self.solar_estimate)/7))),0)

        if (self.ini == 0):
            ta = np.copy(self.price_data)
            for t in range(0,len(ta)):
                if ((t>= self.end_time) and (t<= self.start_time)):
                    ta[t] = 1000
            self.selected_actions = np.delete(np.argsort(ta), np.where(self.solar_estimate>=0.2)[0])[0:self.remaining_chrg]
            return self.selected_actions
        else:
            self.selected_actions = np.delete(np.argsort(-self.theta), np.where(self.solar_estimate>=0.2)[0])[0:self.remaining_chrg]
            return self.selected_actions

    # Updating theta estimate
    def update_estimate(self, t, reward):
        self.reward_history.append(reward)
        reward = self.voltage_filter(reward)
        self.reward_historyv.append(reward)
        reward = self.EV_filter(reward,t)
        self.reward_historye.append(reward)
        self.ini = self.ini + 1
        self.n[t] = self.n[t] + 1
        self.tau0[t] = self.tau0[t] + self.tau[t]*self.n[t]
        self.Q[t] = self.Q[t] + reward
        self.mu0[t] = ((self.tau0[t]*self.mu0[t]) + (self.Q[t]*self.tau[t]))/(self.tau0[t] + self.tau[t]*self.n[t])
        self.theta[t] = np.mean(np.random.normal(self.mu0[t], (1/self.tau0[t]), 1000))

    # Updating estimate of the solar theta
    def update_solar_estimate(self, t, reward):
        self.n_solar[t] = self.n_solar[t] + 1
        self.tau0_solar[t] = self.tau0_solar[t] + self.tau_solar[t]*self.n_solar[t]
        self.Q_solar[t] = self.Q_solar[t] + reward
        self.mu0_solar[t] = ((self.tau0_solar[t]*self.mu0_solar[t]) + (self.Q_solar[t]*self.tau_solar[t]))/(self.tau0_solar[t] + self.tau_solar[t]*self.n_solar[t])
        self.theta_solar[t] = np.mean(np.random.normal(self.mu0_solar[t], (1/self.tau0_solar[t]), 1000))

    # Function to get the average reward of the learning EV agent
    def get_avg_reward(self):
        return np.sum(self.theta[self.selected_actions])/len(self.selected_actions)

    # Reward EV agent based on instataneous price (Use this function if you need this functionality inside the EV agent and not inside the line agent)
    def EV_filter(self,reward,t):
        f_reward = 0
        if (reward != 0):
            f_reward = reward
        else:
            f_reward = (1 - (self.price_data [t]/np.max(self.price_data)))
        return f_reward

    # Method acting as a filter for EV's final reward to ensure voltage stability
    def voltage_filter(self,reward):
        f_reward = 0
        if (self.inst_voltage < 0.95):
            b_reward = 1
        if (self.inst_voltage > 1.05):
            b_reward = -1
        if (self.inst_voltage <= 1.05 and self.inst_voltage >=0.95):
            b_reward = 0

        if (b_reward == 0 or self.cong == True):
            f_reward = reward
        else:
            f_reward = b_reward

        return f_reward