# Importing required libs
import numpy as np
import random

# Line Agent's Class
class Line_Agent:

    def __init__(self, name, rated_current, daily_price, reward_scaling_factor=10):
        self.name = name
        self.rated_current = rated_current
        self.price_data = daily_price
        self.reward_scaling_factor = reward_scaling_factor

    # Method to reward EV agents
    def reward_evs(self, t, current, ev_agents):
        # If there is no congestion in the system
        if(current <= self.rated_current):
            for a in range(0,len(ev_agents)):
                ev_agents[a].cong = False
                if (t in ev_agents[a].selected_actions or t in np.where(ev_agents[a].solar_estimate>=0.2)[0]):
                    reward = 0
                    ev_agents[a].update_estimate(t,reward)
                if (t in np.where(ev_agents[a].theta_solar>=0.2)[0]):
                    ev_agents[a].this_reward = 0
        # If there is congestion in the system
        else:
            for a in range(0,len(ev_agents)):
                ev_agents[a].cong = True
            col_agents = []
            for c1 in range(0,len(ev_agents)):
                if (t in ev_agents[c1].selected_actions or t in np.where(ev_agents[c1].solar_estimate>=0.2)[0]):
                    col_agents.append(ev_agents[c1])
            col_agents = np.array(col_agents)
            random_pick = np.array(random.sample(range(len(col_agents)), int(np.floor(self.rated_current/(current/len(col_agents)))))) if (len(col_agents)>0) else np.array([], dtype = int)
            for rp in range(0,len(random_pick)):
                reward = 0
                col_agents[random_pick[rp]].update_estimate(t,reward)
            for a in range(0,len(col_agents)):
                if (((t in col_agents[a].selected_actions) or t in np.where(col_agents[a].solar_estimate >= 0.2)[0]) and not(a in random_pick)):
                    if (t == 1152):
                        print('ecexuted')
                    reward = -1 * self.reward_scaling_factor
                    col_agents[a].update_estimate(t,reward)