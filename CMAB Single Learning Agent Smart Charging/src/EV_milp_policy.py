# Importing required libs
import numpy as np
#Optimization Modelling Lanuage
import cvxpy as cp

# Defining EV MILP charging policy class
class EV_milp_policy:

    # Constructor
    def __init__(self, daily_decision_instants, selected_study_dumb_evs, selected_study_learning_ev ,naive_ev_charging_policy, p_max, congestion_limit, evs_data_df, selected_pv_profile, electricity_price_original):
        # daily_decision_instants: number of decision making instants in the problem
        # selected_study_dumb_evs: list of index of each non-learning EV
        # selected_study_learning_ev: list of index of the learning EV
        # naive_ev_charging_policy: array of naive charging policy of each EV
        # p_max: rated charging power of the learning EV (in kW)
        # congestion_limit: congestion limit of the problem (in kW)
        # evs_data_df: Dataframe holding EVs data (arrival time, departure time, SoC_i,...)
        # selected_pv_profile: PV profile of a specific day selected to determind naive charging policies of EV agents
        # electricity_price_original: non-normalized electricity price            
        self.daily_decision_instants = daily_decision_instants
        self.selected_study_dumb_evs = selected_study_dumb_evs
        self.selected_study_learning_ev = selected_study_learning_ev
        self.naive_ev_charging_policy = naive_ev_charging_policy
        self.p_max = p_max
        self.congestion_limit = congestion_limit        
        self.evs_data_df = evs_data_df
        self.selected_pv_profile = selected_pv_profile
        self.electricity_price_original = electricity_price_original
        self.learning_agent_power_milp = (cp.Variable(self.daily_decision_instants , boolean  = True)) #Optimization varaible (i.e., instantaneous charging power of the learning EV agent)
        
        
    # Method to execute optimization
    def optimize(self):
        # Constraints for the learning agent optimal policy
        self.constraints = []
        # No congestion constraint (i.e., the sum of charging powers of all EVs should be less than the congestion limit)
        self.constraints.append(((np.sum(self.naive_ev_charging_policy[self.selected_study_dumb_evs],axis=0)*self.p_max) + self.learning_agent_power_milp*self.p_max) <= self.congestion_limit)
        # The learning agent should have the desired amount of SoC at its departure time
        self.constraints.append(cp.sum(self.learning_agent_power_milp) == self.evs_data_df['chrg_req'][self.selected_study_learning_ev[0]])
        # The learning agent should not charge when it is not present at home
        self.ev_depart_slots = np.concatenate((np.linspace(0, self.evs_data_df['t_arrive'][self.selected_study_learning_ev[0]], self.evs_data_df['t_arrive'][self.selected_study_learning_ev[0]] + 1)
                                          , np.linspace(self.evs_data_df['t_depart'][self.selected_study_learning_ev[0]], (self.daily_decision_instants - 1), ((self.daily_decision_instants - 1)-self.evs_data_df['t_depart'][self.selected_study_learning_ev[0]]) +1)
                                          ), axis=None)
        self.constraints.append(self.learning_agent_power_milp[self.ev_depart_slots.astype(int)] == 0)
        
        # Cost function of the optimization problem: At any selected charging instant, the learning EV agent will pay for the electricity it buys from the grid (and not from the PV panels)
        # Note: Electricity price divided by 60 as price is usually given in kWh and we are making decision every minute here. So converting price to kWmin

        self.cost = []
        self.cost.append(cp.maximum(self.learning_agent_power_milp*self.p_max - cp.maximum((self.selected_pv_profile - (np.sum(self.naive_ev_charging_policy[self.selected_study_dumb_evs],axis=0)*self.p_max)),0),0)@(self.electricity_price_original/60))
        # Constructing Complete Objective Function and printing curvature of the cost function, and total number of constraints
        self.total_cost = cp.sum(self.cost)
        # Initializing optimizaiton problem
        self.prob = cp.Problem(cp.Minimize(self.total_cost), self.constraints)
        # Solving the problem
        self.prob.solve(verbose = 0)
        # returning results
        return self.prob.value,self.learning_agent_power_milp 