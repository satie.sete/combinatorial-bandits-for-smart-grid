# Importing required libs
import numpy as np


# Defining EV naive charging policy class
class EVs_naive_policy:

    # Constructor
    def __init__(self, evs_data_df, selected_pv_profile):
        # evs_data_df: Dataframe holding EVs data (arrival time, departure time, SoC_i,...)
        # selected_pv_profile: PV profile of a specific day selected to determind naive charging policies of EV agents
        self.evs_data_df = evs_data_df
        self.selected_pv_profile = selected_pv_profile


    # Method to calculate and return possible charging slots and naive policy of each EV agent in the evs_data_df
    def calculate_naive_policy(self):
        # Create arrays of ones of length len(self.selected_pv_profile) (decision instants) for each EV (len(self.evs_data_df) EVs)
        self.ev_charging_slots = np.zeros((len(self.evs_data_df),len(self.selected_pv_profile)))
        self.ev_charging_policy = np.zeros((len(self.evs_data_df),len(self.selected_pv_profile)))
        # Finding possible EVs charging instants
        for e in range(len(self.evs_data_df)):
            # Set elements between the EV arrival time and departure time equals to False
            self.ev_charging_slots[e][self.evs_data_df['t_arrive'][e]:self.evs_data_df['t_depart'][e]] = True
            # Set elements when EV is going to charge from PV energy to True
            self.ev_charging_policy[e][np.argsort(self.ev_charging_slots[e]  * -self.selected_pv_profile)[:self.evs_data_df['chrg_req'][e]]] = True
        # Returning a numpy array with each EV's possible charging slots and picked naive policy
        return self.ev_charging_slots,self.ev_charging_policy