<p align="center">
  <img width="250" height="100" src="https://user-images.githubusercontent.com/73366653/97095321-45e05a00-165e-11eb-9c60-b90723caba4a.png">
  <img width="250" height="100" src="https://user-images.githubusercontent.com/73366653/97095369-b8513a00-165e-11eb-9ef2-e2fcef626f88.jpeg">
  <img width="150" height="100" src="https://user-images.githubusercontent.com/73366653/97095285-c0f54080-165d-11eb-82bf-e0c032a1e333.png">    
  <img width="110" height="110" src="https://user-images.githubusercontent.com/73366653/97095257-825f8600-165d-11eb-8704-c998a9fae1ce.png"> 
   
</p>

# Combinatorial Bandits for Smart-Grids
This repository contains combinatorial multi-armed bandits (CMAB) based algorithms to optimize energy flows in smart grids. There are following directories in this repository:

 ___
### Simple CMAB
This directory contains CMAB-based algorithms to optimize the charging of electrical batteries in a single-agent learning environment (deterministic and stochastic electricity prices) as well as in a decentralized multi-agent learning environment.

 ___
### CMAB Single Learning Agent Smart Charging
This directory contains the code to optimize the smart charging of a learning electric vehicle agent in an environment consisting of multiple non-learning electric vehicles following a deterministic naive charging policy, and stochastic daily photovoltaic (PV) energy generation. The goal of the learning agent is to minimize its daily charging cost without causing any congestion in the system.

 ___
## Contact
For any further information, you can contact at <14beesharyal@seecs.edu.pk>.
